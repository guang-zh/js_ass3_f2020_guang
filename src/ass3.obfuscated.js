"use strict";
// Guang Zhang 1942372
// JavaScript: assignment3

/**
 * retrieve quotes from the Ron Swanson Quotes API, add it to the ul in the html document
 */

 
let ul; // initialize the ul in the global document
document.addEventListener("DOMContentLoaded", setup);
// define ul again in the setup function and add 'click' to the button for getting quotes from the api
function setup(){
    ul = document.querySelector('ul');
    document.querySelector('#btn').addEventListener('click', getQuote);
}

/**
 * Retrieve the quote from the api and catch, display error messages if failed
 * if quote successfully retrieved, return with json format
 * call for another function addQuote for the returned json of the quote
 * @param {event} e 
 */
function getQuote(e) {
    let promise = fetch('https://ron-swanson-quotes.herokuapp.com/v2/quotes');
    let jsonpromise = promise.then(response => {
        if (!response.ok){
            throw new Error(`HTTP error! status: ${response.status}`);
        } else {
            return response.json();
        }
    }).catch (e =>
        { console.log('Problem with something: ' + e.message);});
    // use response.json() from last step: json from the website response
    jsonpromise.then(json => addQuote(json))
        .catch(e =>
            {console.log('Problem with something: ' + e.message);});
}

/**
 * Create element and associate with the text from Quote and append to the ul in the corresponding div of the document
 * @param {Quote} quote from the API
 */
function addQuote(quote) {
    console.log(quote);
    let newli = document.createElement('li');
    ul.appendChild(newli);
    newli.textContent = quote[0] + '   Author: Ron Swanson';
    console.log(quote[0]);
}
